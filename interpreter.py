import re
VARIABLES = {}
EOF = 'EOF'
ID = 'ID'
OPERATOR = 'OPERATOR'
COMPARATOR = 'COMPARATOR'
INT = 'INT'
INITSTR = 'INITSTR'
FINSTR = 'FINSTR'
STR = 'STR'
PRINT = 'PRINT'
ATTR = 'ATTR'

class Token(object):
    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return 'Token({type}, {value})'.format(
            type = self.type, 
            value = self.value
            )

    def __repr__(self):
        return self.__str__()

class Interpreter(object):
    def __init__(self, entry):
        self.entry = entry
        self.position = 0
        self.current_token = None
        self.init_str = False

    def error(self):
        raise Exception('Error parsing output')

    def get_next_token(self):
        entry = self.entry

        if self.position > len(entry) - 1:
            token = Token(EOF, None)
            return token

        current_char = entry[self.position]

        if current_char.isdigit() and self.init_str == False:
            token = Token(INT, int(current_char))
            self.position += 1
            return token

        if current_char == '@' and self.init_str == False:
            token = Token(PRINT, current_char)
            self.position += 1
            return token

        if re.match(r'[a-zA-Z0-9\_]', current_char) and self.init_str == False:
            token = Token(ID, current_char)
            self.position += 1
            return token

        if re.match(r'[\=]', current_char) and self.init_str == False:
            token = Token(ATTR, current_char)
            self.position += 1
            return token

        if re.match(r'^([\+\-\/\%\*])', current_char) != None and self.init_str == False:
            token = Token(OPERATOR, current_char)
            self.position += 1
            return token

        if re.match(r'[\'|\"]', str(current_char)) != None and self.init_str == False:
            self.init_str = True
            token = Token(INITSTR, current_char)
            self.position += 1
            return token

        if re.match(r'[a-zA-Z0-9\u00C0-\u00FF\+\-\=\/\@\!\? ]', current_char) and self.init_str == True:
            token = Token(STR, current_char)
            self.position += 1
            return token

        if re.match(r'[\'|\"]', str(current_char)) != None and self.init_str == True:
            token = Token(FINSTR, current_char)
            self.position += 1
            self.init_str = False
            return token

        self.error()
    
    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.get_next_token()
        else:
            self.error()

    def expr(self):
        self.current_token = self.get_next_token()

        primeToken = self.current_token
        if primeToken.type == PRINT:
            return self.definePrint()
        
        if primeToken.type == ID:
            return self.defineId()

    def definePrint(self):
        self.eat(PRINT)

        if self.current_token.type == INITSTR: 
            self.eat(INITSTR)

            string = {}
            counter = 0
            while self.current_token.type == STR:
                string[counter] = self.current_token.value
                counter += 1
                self.eat(STR)
            
            stringFormed = ''.join(str(value) for key, value in string.items())

            fin_str = self.current_token
            self.eat(FINSTR)

            return stringFormed

        elif self.current_token.type == ID:
            ids = {}
            counter = 0
            while self.current_token.type == ID:
                ids[counter] = self.current_token.value
                counter += 1
                self.eat(ID)

            idFormed = ''.join(str(value) for key, value in ids.items())

            if VARIABLES.get(idFormed) != None:
                return VARIABLES.get(idFormed)
            else:
                return 'This variable isn\'t exists'

    """Define a função para definição de variáveis em um dicionário"""

    def defineId(self):
        ids = {}
        counter = 0
        while self.current_token.type == ID:
            ids[counter] = self.current_token.value
            counter += 1
            self.eat(ID)

        idFormed = ''.join(str(value) for key, value in ids.items())

        attr = self.current_token
        self.eat(ATTR)

        init_str = self.current_token
        self.eat(INITSTR)

        string = {}
        counter = 0
        while self.current_token.type == STR:
            string[counter] = self.current_token.value
            counter += 1
            self.eat(STR)
        
        stringFormed = ''.join(str(value) for key, value in string.items())

        fin_str = self.current_token
        self.eat(FINSTR)

        VARIABLES.update({idFormed: stringFormed})

        return VARIABLES.get(idFormed)

def main():
    while True:
        try:
            entry = input('ConscScript > ')
        except EOFError:
            break
        if not entry:
            continue 
        interpreter = Interpreter(entry)
        result = interpreter.expr()
        print(result)

if __name__ == '__main__':
    main()